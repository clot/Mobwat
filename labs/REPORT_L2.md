# Lab n°2 - Performance evaluation of EDCA MAC

13/04/18 - William CLOT

**Summary:**
1. [Sender Model](#sender-model)
  - Simulation of EDCA internal contention
  - Simulation of general contention
2. [Fading Models](#fading-models)
3. [Modulation and coding](#modulation-and-coding)

------------

## Sender Model

#### Simulation of EDCA internal contention

In this situation we will be looking at the behavior of IDCA 802.11p in case of internal contention. We will be using the case scenario `scen_priority_ic.cc` where the car (1) will be sending information (DENM & CAM packets) to the car 0.

![situation_load](lab2_files/6-1.png)

I simulated this case scenario with a varying data rate from 1Mbps to 3Mbps. I also used the `awk` program to find the average delay.

**explanation of the awk program:**

```bash
 awk '{ s += $2 } END { print "sum: ", s, " average: ", s/NR, " samples: ", NR }' delayXXXX.dat
```

the argument `s+= $2` will take the second word of each line and add it all together in order to get the sum of the delay. Then the average is calculated by dividing `s` by the number of lines in the `.dat` file which is `NR`.  

**Question 1**:
After simulating all the data rates between 1Mbps and 3Mbps, I was able to calculated the `PRR` by dividing the number of packets received and the number of packets sent like so:

This table represents my results for the `PRR` of `DENM` packets:

*data rate (Mbps)*|*DENM packets sent*|*DENM packets received*|*PRR DENM*
-:                |:-:                |:-:                    |:-:
**1**           	|	2499              | 2499                  | 1
**1.5**	          |	3749              | 3748                  | 0.999
**2**             | 4998              | 4545                  | 0.909
**2.5**           | 6248              | 4545                  | 0.727
**3**             | 7498              | 4546                  | 0.606

Which gives us the following graph like so:

| ![situation_load](lab2_files/png_files/6-1/PRRDenm.png) |
|:--:|
| *PRR of DENM packets* |



This table represents my results for the `PRR` of `CAM` packets:

*data rate (Mbps)*|*CAM packets sent*|*CAM packets received*|*PRR CAM*
-:                |:-:                |:-:                    |:-:
**1**           	|	2499              | 2499                  | 1
**1.5**	          |	3750              | 2732                  | 0.729
**2**             | 4999              | 1959                  | 0.392
**2.5**           | 6249              | 1959                  | 0.313
**3**             | 7500              | 1959                  | 0.261

Which gives us the following graph like so:

| ![situation_load](lab2_files/png_files/6-1/PRRCam.png) |
|:--:|
| *PRR of CAM packets* |

By comparing both graphs we can see that the more the data rate is high the more the `PRR` of both packets will slowly be lowered down. Nonetheless, we can see that the `DENM` packets keep a good value at 1 until 1.5Mbps and compared to the same relative values of `CAM` the `PRR` of `DENM` packets are higher. This translates the fact that the 802.11p protocol priorities the most important packets: the **DENM packets**.


I then ploted the variation of the average delay after using the awk program, the average delay represents the time spent between the packet is sent and then received by the application layer on the receiving node:


| ![situation_load](lab2_files/png_files/6-1/averagedelayDenm.png) |
|:--:|
| *Average delay of DENM packets* |

| ![situation_load](lab2_files/png_files/6-1/averagedelayCam.png) |
|:--:|
| *Average delay of CAM packets* |

For the `DENM` packets we can see that the delay is amost 0 for small data rates inferior to 1.5Mbps. It then increases until reaching a maximum value around 3Mbps at 1.6. For the `CAM` packets the delay increases from data rates starting at 1Mbps and reaches a maximum value of 3.5. We can see here that the `DENM` packets have priority here because the delay for `DENM` packets is really low for small bit rates but the delay increases even at small bit rates for the `CAM` packets. Moreover the value of the peak delay is higher for the `CAM` packets then the `DENM` packets.

**Question 2**:
After opening up the `.pcap` files I found that the in the header of the packets the value of QoS. The value of QoS is either :
- `001` Priority: Background (1)
- `011` Priority: Excellent Effort (Best Effort) (3)

We can see two types of packets sent, some with low priority and some with high priority. This can actually be translated to the difference between the `Cam` and `Denm` packets, the `Denm` are sent with high priority (best effort) and the `Cam` packets are sent with background priority.

#### Simulation of general contention

In this simulation we will be looking at the situation were 10 cars are trying to communicate to 1 car in particular. We will look the consequences of this network load at node 0.

![situation_load](lab2_files/6-2.png)

**Question 1**:
I increased the data rate along these values: 0.1Mbps, 0.5Mbps, 1Mbps, 2Mbps with this new scenario and got the following results:

*data rate (Mbps)*|*DENM packets sent*|*DENM packets received*|*CAM packets sent*|*CAM packets received*
-:|:-:|:-:|:-:|:-:
**0.1**    |	249   | 232.2 |	 249   | 215.3
**0.5**	   |	1248  | 549.5 |	 1248  | 14.6
**1**      |  2496  | 550.3 |  2496  | 15.9
**2**      |  4993  | 552.4 |  4993  | 14.9

We can calculated easily the fraction of packets received over packets sent and get the PRR as so:

*data rate (Mbps)*|*DENM PRR*|*CAM PRR*
-:|:-:|:-:
0.1|0.9325|0.8646
0.5|0.4403|0.0117
1|0.2204|0.00637
2|0.1106|0.0029842

**Question 2**: We then get the following plots:

| ![situation_load](lab2_files/png_files/6-2/PRRDenm.png) |
|:--:|
| *PRR of DENM packets* |

| ![situation_load](lab2_files/png_files/6-2/PRRCam.png) |
|:--:|
| *PRR of CAM packets* |

**Question 3**: We can see that in this situation where a large number of cars are communicating to the same car with a high data rate the `CAM` packet's PRR rapidly goes down and get to a level near of 0 for data rates above 0.5Mbps. The decrease of these `CAM` packets is linear. Furthermore if we look at the graph for the more important `DENM` packets the PRR decreases in a slower logarithmic patern. We can see that these packets still have the priority over the channel when the data rate increases.

## Fading models

In this situation we will be looking at this scenario with 2 cars but with a mouvement in one of two cars. This mouvement will add fading to the signal and will alter the signal received by the mouving car.

![situation_load](lab2_files/7.png)

**Question 1**: I started the simulation taking into account only the the *log-distance* contribution of the fading.


| ![situation_load](lab2_files/png_files/7/logdistance.png) |
|:--:|
| *Log Distance fading* |

We can see that at certain distance the probability of reception decreases drastically. This distance is around the value 120.

**Question2**: I then added the following lines of code in the `scen_fading.cc` file to add the *log-normal* contribution of the fading:

```c++
/////// Log Normal /////
wifiChannel.AddPropagationLoss ("ns3::RandomPropagationLossModel");
Config::SetDefault ("ns3::RandomPropagationLossModel::Variable",
  StringValue ("ns3::NormalRandomVariable[Mean=0.0|Variance=9.0]"));

Experiment experiment2 = Experiment ("Lognormal");
dataset = experiment2.Run (wifi, wifiPhy, wifiMac, wifiChannel);
gnuplot.AddDataset (dataset);
```


I then was able to plot the *log-distance* and *log-normal* fading on the same plot as so:

| ![situation_load](lab2_files/png_files/7/lognormal.png) |
|:--:|
| *Log Normal fading* |

The *log-normal* fading adds oscillations around the *log-distance* fading. In this case it as added big oscillations and smoothed out the curve of probability of reception. We can see the oscillations at the end of the curve at distances above 150.


**Question 3**: I then needed to add the different cases of *Nakagami* fading. I modified again the `scen_fading.cc` file as so:

```c++
/////// Nakagami //////
 wifiChannel.AddPropagationLoss ("ns3::NakagamiPropagationLossModel");

 Config::SetDefault ("ns3::NakagamiPropagationLossModel::m0", DoubleValue(1));
 Config::SetDefault ("ns3::NakagamiPropagationLossModel::m1", DoubleValue(1));
 Config::SetDefault ("ns3::NakagamiPropagationLossModel::m2", DoubleValue(1));

 Experiment experiment3 = Experiment ("Nakagami m=1 NLOS conditions");
 dataset = experiment3.Run (wifi, wifiPhy, wifiMac, wifiChannel);
 gnuplot.AddDataset (dataset);


 wifiChannel.AddPropagationLoss ("ns3::NakagamiPropagationLossModel");
 Config::SetDefault ("ns3::NakagamiPropagationLossModel::m0", DoubleValue(3));
 Config::SetDefault ("ns3::NakagamiPropagationLossModel::m1", DoubleValue(3));
 Config::SetDefault ("ns3::NakagamiPropagationLossModel::m2", DoubleValue(3));
 Experiment experiment4 = Experiment ("Nakagami m=3 LOS conditions");
 dataset = experiment4.Run (wifi, wifiPhy, wifiMac, wifiChannel);
 gnuplot.AddDataset (dataset);

 wifiChannel.AddPropagationLoss ("ns3::NakagamiPropagationLossModel");
 Config::SetDefault ("ns3::NakagamiPropagationLossModel::m0", DoubleValue(5));
 Config::SetDefault ("ns3::NakagamiPropagationLossModel::m1", DoubleValue(5));
 Config::SetDefault ("ns3::NakagamiPropagationLossModel::m2", DoubleValue(5));
 Experiment experiment5 = Experiment ("Nakagami m=5 LOS conditions");
 dataset = experiment5.Run (wifi, wifiPhy, wifiMac, wifiChannel);
 gnuplot.AddDataset (dataset);

 wifiChannel.AddPropagationLoss ("ns3::NakagamiPropagationLossModel");
 Config::SetDefault ("ns3::NakagamiPropagationLossModel::m0", DoubleValue(7));
 Config::SetDefault ("ns3::NakagamiPropagationLossModel::m1", DoubleValue(7));
 Config::SetDefault ("ns3::NakagamiPropagationLossModel::m2", DoubleValue(7));
 Experiment experiment6 = Experiment ("Nakagami m=7 LOS conditions");
 dataset = experiment6.Run (wifi, wifiPhy, wifiMac, wifiChannel);
 gnuplot.AddDataset (dataset);

 wifiChannel.AddPropagationLoss ("ns3::NakagamiPropagationLossModel");
 Config::SetDefault ("ns3::NakagamiPropagationLossModel::m0", DoubleValue(5));
 Config::SetDefault ("ns3::NakagamiPropagationLossModel::m1", DoubleValue(3));
 Config::SetDefault ("ns3::NakagamiPropagationLossModel::m2", DoubleValue(1));
 Experiment experiment7 = Experiment ("Nakagami mix");
 dataset = experiment7.Run (wifi, wifiPhy, wifiMac, wifiChannel);
 gnuplot.AddDataset (dataset);
```

I was then able to plot all these different fading options:

| ![situation_load](lab2_files/png_files/7/nakagami.png) |
|:--:|
| *Nakagami fading* |

The *nakagami* adds even more fading in these situations. The fading starts at smaller distances with m=1 then with higher values of m. The fading in all these situations will effect the properties of a wireless link. This is one of the limitation of such wireless communications. At distances above 70 the probability of reception will plumit down very quickly and it will not be feasable to communicate with this channel.

## Modulation and Coding

In this situation we will investigate the reception capabilities depending on the modulation of the signal.

![situation_load](lab2_files/6-1.png)

We will try all the following modulations schemes:
- BPSK – 3 Mbps (coding rate: 1⁄2)
- QPSK – 6 Mbps (coding rate: 1⁄2)
- QPSK – 9 Mbps (coding rate: 3⁄4)
- 16-QAM – 12 Mbps (coding rate: 1⁄2)
- 16-QAM – 18 Mbps (coding rate: 3⁄4)
- 64 QAM – 24 Mbps (coding rate: 2⁄3)
- 64 QAM – 27 Mbps (coding rate: 3⁄4)

**Question1**: I went into the file `scen_data_rate.cc` and modified the different evalutions to take into account all the modulation schemes:


```c++
/// 6 Mbs data rate ///
NS_LOG_DEBUG ("6");
experiment2 = Experiment ("6Mb");
wifi.SetRemoteStationManager ("ns3::ConstantRateWifiManager",
                              "DataMode", StringValue ("OfdmRate6MbpsBW10MHz"));
dataset = experiment2.Run (wifi, wifiPhy, wifiMac, wifiChannel);
gnuplot.AddDataset (dataset);

/// 9 Mbs data rate ///
NS_LOG_DEBUG ("9");
experiment3 = Experiment ("9Mb");
wifi.SetRemoteStationManager ("ns3::ConstantRateWifiManager",
                              "DataMode", StringValue ("OfdmRate9MbpsBW10MHz"));
dataset = experiment3.Run (wifi, wifiPhy, wifiMac, wifiChannel);
gnuplot.AddDataset (dataset);

/// 12 Mbs data rate ///
NS_LOG_DEBUG ("12");
experiment4 = Experiment ("12Mb");
wifi.SetRemoteStationManager ("ns3::ConstantRateWifiManager",
                              "DataMode", StringValue ("OfdmRate12MbpsBW10MHz"));
dataset = experiment4.Run (wifi, wifiPhy, wifiMac, wifiChannel);
gnuplot.AddDataset (dataset);

/// 18 Mbs data rate ///
NS_LOG_DEBUG ("18");
experiment5 = Experiment ("18Mb");
wifi.SetRemoteStationManager ("ns3::ConstantRateWifiManager",
                              "DataMode", StringValue ("OfdmRate18MbpsBW10MHz"));
dataset = experiment5.Run (wifi, wifiPhy, wifiMac, wifiChannel);
gnuplot.AddDataset (dataset);

/// 24 Mbs data rate ///
NS_LOG_DEBUG ("24");
experiment6 = Experiment ("24Mb");
wifi.SetRemoteStationManager ("ns3::ConstantRateWifiManager",
                              "DataMode", StringValue ("OfdmRate24MbpsBW10MHz"));
dataset = experiment6.Run (wifi, wifiPhy, wifiMac, wifiChannel);
gnuplot.AddDataset (dataset);

/// 27 Mbs data rate ///
NS_LOG_DEBUG ("27");
experiment7 = Experiment ("27Mb");
wifi.SetRemoteStationManager ("ns3::ConstantRateWifiManager",
                              "DataMode", StringValue ("OfdmRate27MbpsBW10MHz"));
dataset = experiment7.Run (wifi, wifiPhy, wifiMac, wifiChannel);
gnuplot.AddDataset (dataset);
```

I then got the following plot:

| ![situation_load](lab2_files/png_files/8/datarate.png) |
|:--:|
| *Modulation schemes* |

Either the PRR is really high but for small distance (ie 27Mb) or the PRR is smaller but we will be able to communicate for longer distances (ie 3Mb). This is the trade-off that communication engineers must deal with, find the best compromise between range of signal and reception quality (PRR).


**Question2**: I added the corresponding fading models to the simulation and got the following plot:

| ![situation_load](lab2_files/png_files/8/datarate_fade.png) |
|:--:|
| *Modulation schemes with fading model* |
