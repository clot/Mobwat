# Lab n°1 - Performance evaluation of IEEE 802.11 MAC protocol

06/04/18 - William CLOT

**Summary:**
1. [Impact of network load](#impact-of-network-load)
2. [Hidden nodes problem](#hidden-nodes-problem)
3. [Distance and Packets size impact](#distance-and-packet-size-impact)

------------

## Impact of network load

![situation_load](lab1_files/load.png)

**Question 1**: Simulation with `load_1.cc`

I used these commands to simulated and then extract the data to plot the figure. First to simulate:

```bash
./waf --run “scratch/load1 --bitrate=BITRATE --ascii=out_load_1_BITRATE.tr -- wireshark= out_load_1_BITRATE”
```
then `awk` to extract the data as so:

```bash
awk –f tools/goodput_average_delay.awk out_load_1_BITRATE.tr
```

*(with a varying bitrate from 0.1Mbps to 1Mbps.)*

I then got the following graph by reporting the values in the [`load_1.dat`](lab1_files/load_1.dat) file.

![first graph](lab1_files/load_1.png)

We can see that the goodput is linear at first with the data rate. Nonetheless there is a cap at the end of the graph starting from the data rate 0.8Mbps. From 0.8Mbps and upwords the goodput is increasing less and less and will probably reach a maximum after 1Mbps. We can conclude with this load that that the impact of network load on the IEEE 802.11 MAC protocol will effect the performance.

**Question 2**: Simulation with `load_2.cc`

I did the same commands and ns simulation with the new `load_2.cc` file and reported my results in the [`load_2.dat`](lab1_files/load_2.dat) file. I then changed the input/output of the Gnuplot script to obtain the following graph:

![first graph](lab1_files/load_2.png)

We can see this time that we have much more flexibility in the data rate before saturating the goodput. There is a linear area from 0 to 10Mbps and then it slowly decreases between 10Mbps and 25Mbps.

## Hidden nodes problem

![situation_hidden](lab1_files/hidden.png)

**Question 1: With RTS/CTS disabled (threshold at 2200bytes)**

##### What kind of frames are there ?

**From the point of view of node1:**
We can see that the node0 at address 10.0.0.1 sends several data frames to node1 at address 10.0.0.2. The node2 at address 10.0.0.3 sends also data frames to node1 at the same time. As node0 and node2 don't detect each other they don't detect the transmission of there relative nodes to node1 (ie node2 doesn't detect the transmission from node0 and node0 the transmission of node2). Therefore there are collisions at node1 and he is unable to decrypt some of the packets: there is a lot a lost packets in the transmission. The two send back their packets with the unique sequence number until they receive an ACK of node1.

*There are mostly data packets and control packet (ACK).*

**From the point of view of node0:**
This time there are not packets sent by node2 which is normal because the node0 doesn't receive any data sent by node2. There are only communications between node0 and node1.
Node0 tries to send it's data packets to node1 but there are collision and he doesn't receive any ackowledgement from node1 so node0 resends the same sequence packet until he receives an ACK and then sends the next sequence packet.

*Here again there are mostly data packets from node0 and ACK packets from node1.*

##### Where can we see that there were collisions?

The collision occur at node1 physically, but we can see the collision on the wireshark files at node0. The collisions can be seen by the fact that node0 has to send several times the same sequence packet. Each packet sent with no ACK in return is a collision at node1 and he is therefore unable to decrypt the message sent.


##### What is the throughput?

After finishing the simulation I have the following results:

*from node*|*to node*|*throughput*
:-:|:-:|:-:
**0**	|	**1** | 0.233557 Mbps
**2**	|	**1** | 0.338912 Mbps


The throughput is low which is normal because node0 and node2 have to send several times the same sequence packets.

**Question 2: With RTS/CTS enabled (Threshold at 150bytes)**

#### What kind of frames are there ?

**From the point of view of node1:**

There are some new type of RTS packets: (request-to-send and clear to send). The node0 and node2 send `request to send` packets to node1 and then they can receive `clear to send` packets and then the relative node takes over the channel and is then able to send the packets correctly avoiding collisions.

**From the point of viez of node0:**

Everything is pretty much the same then from the view of node1. The node0 sends a handshake `request to send` then receives a `clear to send` (depending on the transmission of the hidden node2) and then sends the data packet and waits for an `ACK` packet.

#### Which packets help to detect the presence of a hidden node?

We can see that there is a hidden node when the node1 sends to **both** node0 and node2 the `clear to send` for node2 which means that the channel is allocated to a hidden node2. The node0 then waits for a certain time before sending another `request to send` packet.

#### What is the throughput?

After finishing the simulation we have for 0 --> 1  Throughput: 0.729867 Mbps

and for 2 --> 1 Throughput: 0.599125 Mbps

This throughtput is much better then previously. The RTS/CTS helped a lot to take collisions away and increase the throughtput.


## Distance and packets size impact

![situation_distance](lab1_files/distance.png)

**Question 1**
 After modifying the distance in the ns simulation I obtained the following curve:

![first graph](lab1_files/load_3.png)

The goodput is maximal for distances inferior to 120 with a value of 500000, but then the goodput decreases linearly from this maximum value to 0 at the distance of 160. We can see that the distance doesn't impact performances for small distances inferior to 120 but then effects drastically the performance.

Here is a table of the lost packets depending on the distance:

*distant*|*lost packets*
 -:|:-:
**80**	|	1
**100**	|	1
**120**	|	1
**140**	|	141
**160**	|	281
**180**	|	281

To determine the distance at which the packet losses are maximum but before being out of range I simulated the same simulation by changing the distance until I found a packet losse of 281. I knew already that I should be looking at a distance between 140 and 160.

After simulating several distances I found that at distance 151 the number of lost pakets is 278 and at distance 252 the number of lost packets is 281 (all of them).

So the maximum distance before being out of range is: **D = 151**. In the configuration the throughput is of 0.01.

**Question 2** After simulating with the distance of 151 and a bitrate of 0.5Mbps I found the following results:

*from node*|*goodput*|*throughput*
-:|:-:|:-:
**1**	|	499733 | 0.99
**2**	|	100933 | 0.2

Obviously as node1 hasn't changed position so the throughput is still very good. The goodput for node2 is better with a smaller packet size. With a packet size of 2000 I had a throughput of 0.01 which is 20 times smaller than we packet sizes of 150.

*We can see that with long ditances the packet size is an important factor in the performance of IEEE 802.11 mac protocol.*

After simulating with the distance of 151 and a bitrate of 0.05Mbps I found the following results:

*from node*|*goodput*|*throughput*
-:|:-:|:-:
**1**	|	49866 | 1
**2**	|	49333 | 0.98

The throughput increases drastically with long distances situations when the bitrate is small. We see that only 4 packet are lost here.


**Question 3**: I changed the value of `MaxSlrc` and got the following results:

*MaxSlrc*|*throughput*|*goodput*
-:|:-:|:-:
**1**	|	0 | 0
**7**	|	0.0714 | 5333

We can see that the value of MaxSlrc does effect the value of the goodput. If we can try to send packets several times some packets that could have been lost will still be received by node0.

*The more the MaxSlrc is high the more chances there will be to send packets to far distances and the more the throughput will be high.*
