  /// 6 Mbs data rate ///
  NS_LOG_DEBUG ("6");
  experiment2 = Experiment ("6Mb");
  wifi.SetRemoteStationManager ("ns3::ConstantRateWifiManager",
                                "DataMode", StringValue ("OfdmRate6MbpsBW10MHz"));
  dataset = experiment2.Run (wifi, wifiPhy, wifiMac, wifiChannel);
  gnuplot.AddDataset (dataset);

  /// 9 Mbs data rate ///
  NS_LOG_DEBUG ("9");
  experiment3 = Experiment ("9Mb");
  wifi.SetRemoteStationManager ("ns3::ConstantRateWifiManager",
                                "DataMode", StringValue ("OfdmRate9MbpsBW10MHz"));
  dataset = experiment3.Run (wifi, wifiPhy, wifiMac, wifiChannel);
  gnuplot.AddDataset (dataset);

  /// 12 Mbs data rate ///
  NS_LOG_DEBUG ("12");
  experiment4 = Experiment ("12Mb");
  wifi.SetRemoteStationManager ("ns3::ConstantRateWifiManager",
                                "DataMode", StringValue ("OfdmRate12MbpsBW10MHz"));
  dataset = experiment4.Run (wifi, wifiPhy, wifiMac, wifiChannel);
  gnuplot.AddDataset (dataset);

  /// 18 Mbs data rate ///
  NS_LOG_DEBUG ("18");
  experiment5 = Experiment ("18Mb");
  wifi.SetRemoteStationManager ("ns3::ConstantRateWifiManager",
                                "DataMode", StringValue ("OfdmRate18MbpsBW10MHz"));
  dataset = experiment5.Run (wifi, wifiPhy, wifiMac, wifiChannel);
  gnuplot.AddDataset (dataset);

  /// 24 Mbs data rate ///
  NS_LOG_DEBUG ("24");
  experiment6 = Experiment ("24Mb");
  wifi.SetRemoteStationManager ("ns3::ConstantRateWifiManager",
                                "DataMode", StringValue ("OfdmRate24MbpsBW10MHz"));
  dataset = experiment6.Run (wifi, wifiPhy, wifiMac, wifiChannel);
  gnuplot.AddDataset (dataset);

  /// 27 Mbs data rate ///
  NS_LOG_DEBUG ("27");
  experiment7 = Experiment ("27Mb");
  wifi.SetRemoteStationManager ("ns3::ConstantRateWifiManager",
                                "DataMode", StringValue ("OfdmRate27MbpsBW10MHz"));
  dataset = experiment7.Run (wifi, wifiPhy, wifiMac, wifiChannel);
  gnuplot.AddDataset (dataset);

