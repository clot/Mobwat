  /////// Log Normal /////
  wifiChannel.AddPropagationLoss ("ns3::RandomPropagationLossModel");
  Config::SetDefault ("ns3::RandomPropagationLossModel::Variable",
    StringValue ("ns3::NormalRandomVariable[Mean=0.0|Variance=9.0]"));
  
  Experiment experiment2 = Experiment ("Lognormal");
  dataset = experiment2.Run (wifi, wifiPhy, wifiMac, wifiChannel);
  gnuplot.AddDataset (dataset);
