# Mobile Wireless Access Technologies (MOBWAT)

Course from EURECOM, Jérôme HÄRRI. 2018


- **Introduction to wifi, wireless MAC & the IEEE 802.11 family**
  - [notes](notes/1-intro_wlan.md)
  - [slides](slides/1-intro_wlan.pdf)
- **IEEE 802.11 MAC Functions**
  - [notes](notes/2-802_11_mac.md)
  - [slides](notes/2-802_11_mac.md)
- **IEEE 802.11 PHY**

Notes taken by William CLOT
