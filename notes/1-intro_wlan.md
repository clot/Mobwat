# Introduction to Wifi & WLAN

Course from EURECOM, Jérôme HÄRRI. 2018

**Summary**:
- [General Introduction](#general-introduction)
- Spectrum Management
- Wireless Networking Principles
- Classification of Medium Access Types
- Random Access Protocols
- IEEE 802.11 Family

-------------

## General Introduction

The Wifi market is fastly growing marker (over 5.8M hotspots) in 2015. The number of devices supported is also growing fastly.
The Wifi *at the beginning* was used for corporate campuses or offices, hotspots. *Today* the use of Wifi as widly spread, with streaming networking and consumer electronics.
*Tomorrow* the Wifi will be used for car to car infrastructures etc..

Why use the wifi ? It's **cheap**, secured, has dynamic standards. But it's also very flexible (temporary installations, no cabling).

So what is WLAN ? A Local Area Network is a shared access to a common transmission channel without billing. A WLAN is a LAN without wires, the traffic are alike.

The WLAN is present in the *Local Area* network but also the *Wide Area* (with Wifi mesh) and the *Personal Area* (with Wifi direct).

WLAN uses **radio medium** to exchange data.

## Spectrum Management

Wifi uses *unlicensed spectrum* 

* CHANNEL ALLOCATION IN 2.4GHz BAND :
  - Channel from 1 to 13 overlapping (22MHz wide)

* CHANNEL ALLOCATION IN 5GHz BAND :
  - WLAN indoor bands
  - RLAN outdoor bands
  - dedicated ITS (intelligent transportation system) bands (dedicated to car to car connections)


* MEDIUM ACCESS CONTROL (MAC) :
  - Trying to efficiently avoid collisions and maximize the bandwidth
  - Beaming is a solution to avoid collisions
  - Need to be aware in omnidirectional to manage the unidirectional transmission (communication)

* SINR : Signal to interference ratio
  - Several levels : noise level, sensing threshold, decoding threshold


## Wireless Networking Principles

## Classification of Medium Access Types

## Random Access Protocols

## IEEE 802.11 Family

### Forming a Wireless Network: Architecture
 - Basic Service Set (BSS) : A station must always join a BSS (except vehicules) even in direct wifi (adhoc)
 - Extended Service Set (ESS) : Linking APs with a set of BSSs (Eduroam)
 - The BSS is under the control of the AP (throughput is divided by 2), but AP is providing services (sleep mode, security...)
 - `Service Set Identifier` (SSID), is the network name (ex: blilbi) and can have several `Basic Service Set Identifier` (BSSID) is
