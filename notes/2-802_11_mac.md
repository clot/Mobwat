# IEEE 802.11 MAC Functions

## I. MAC layer Management

### Synchronization
* To join a 802.11 network, there's a scanning+sync phase then an authentication, association and DHCP.
* All stations within a BSS are `synchronized` to a common clock. In infrustucture mode the AP is the time master. in Adhoc mode each station adopts the timing received from every beacon.

### Association
* Each BSS as only one AP.
* The BSS sends a `probe request` in broadcast and every AP sends back a `probe reply` then the BSS sends `association request` to chooses the best AP and receives a `association response` from the choosen AP.

### Power Management
* Energy must be saved (smartphone, laptop), IEEE 802.11 containes power saving mechanisms. The station goes to sleep (the wifi card) as soon a possible and the AP buffers it's data.
* Sends TIM (traffic indicator map) to tell which frames are buffered.
* The station wakes up periodically and listens to beacons can decode the TIM
* Then sends a `PS-POLL` to the AP to request the buffered data.
* If it's a DTIM no need to send a `PS-POLL` because it's in broadcast. There needs to be a compromise between power management and transmission.

### MAC Frames Format and addressing
* Generic frames : MAC header + body frame(0-23124bits)
* Control frames : RTS, CTS and ACK
* See paper schemas.

|  To DS  |  From DS  |  Addr 1  |  Addr 2  |  Addr 3  |  Addr 4  |
|:---     |:---       |:---      |:---      |:---      |:---      |
|1  or  0 | 1  or  0    | Destination address | Source address | BSSID of AP | Access Point Destination (only for Distributed Systems) |



## II. Distributed Coordination Function (DCF)

## III. Hybrid Coordination Function (HCF) QoS
